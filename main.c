#include <stdio.h>
#include <stdint.h>
#include <assert.h>

// design philosophy
// no optimization for multi-threading memory allocation
// high spacial efficiency
// can be trained, TODO

// questions? when free, set all area to 0 or not?
// what if the ptr is not generated from malloc?
// does not deal with double free or error data

typedef int i32_t;
typedef unsigned int u32_t;
typedef unsigned long long int u64_t;
typedef long long int i64_t;
// typedef unsigned int size_t;
typedef int bool_t;
typedef int errno_t; // error number

// #define NULL bool_t->NULL
// #define NULL (0)

#define MALLOC_ALIGN

#define SHIFT_BITS 3
// https://scaryreasoner.wordpress.com/2009/02/28/checking-sizeof-at-compile-time/
#define BUILD_ERROR(condition) ((void)sizeof(char[1 - 2*!!(condition)]))

enum ERRNO {
    OK = 0,
    ERR_POS_NOT_ALIGN = -1,
    ERR_NO_ENOUGH_SPACE = -2,
    ERR_SIZE_OVERFLOW = -3,
    ERR_PTR_NOT_FOUND = -4
};


void MEMDBG_free (const void *, char *, int);

#ifdef MALLOC_ALIGN
typedef __attribute__ ((aligned ((1 << SHIFT_BITS)))) struct allocated_memory {
    struct allocated_memory *next_area __attribute__ ((aligned ((1 << SHIFT_BITS))));
    size_t size __attribute__ ((aligned ((1 << SHIFT_BITS))));
    struct allocated_memory *self __attribute__ ((aligned ((1 << SHIFT_BITS))));
} allocated_memory_t ;
#else
typedef struct allocated_memory {
    struct allocated_memory *next_area;
    size_t size;
    struct allocated_memory *self;
} allocated_memory_t;
#endif


// int printf (const char *format, ...) {
//     return 0;
// }

#define MALLOC_SIZE (1024)
#ifdef MALLOC_ALIGN
u32_t __attribute__ ((aligned ((1 << SHIFT_BITS)))) g_malloc_area[MALLOC_SIZE] = {0} ;
#else
u32_t g_malloc_area[MALLOC_SIZE] = {0};
#endif
allocated_memory_t *g_root;
int g_error_no = 0;


uintptr_t ceiling_to_2_pow_n (uintptr_t a, int bit) {
    a = a == (a >> bit << bit) ? a : ((a >> bit) + 1) << bit;
    return a;
}


// O(n)
void my_free (const void *ptr) {
    if (!g_root) {
        g_root = (allocated_memory_t *)g_malloc_area;
        assert (0 && "Call free before call malloc once,");
    }

    if (!ptr) {
        return;
    }

    allocated_memory_t *iter = (allocated_memory_t *) ((uintptr_t)ptr - sizeof (allocated_memory_t));
    assert ((void *)iter->self == ptr);
    assert ((uintptr_t)iter->self - (uintptr_t)iter == sizeof (allocated_memory_t));

    // works with last area, the iter->next_area is simply 0;
    allocated_memory_t *previous = (allocated_memory_t *)g_root;

    if (iter == g_root) {
        if (iter->next_area) {
            g_root = iter->next_area;
            goto my_free_release_iter;
        }
    }
    else { // iter is not g_root
        while (previous->next_area) {
            if (previous->next_area == iter) {
                previous->next_area = iter->next_area;
                goto my_free_release_iter;
            }
            previous = previous->next_area;
        }
        goto my_free_error_did_not_find_previous;
    }

my_free_release_iter:
    iter->next_area = (allocated_memory_t *)0;
    iter->size = 0;
    iter->self = (allocated_memory_t *)0;
    return;
my_free_error_did_not_find_previous:
    assert (0 && "my_free_error_did_not_find_previous");
}

// O(n)
// side effect, modify g_root
void *my_malloc (size_t asked_size) {

#ifdef MALLOC_ALIGN
    BUILD_ERROR (sizeof (uintptr_t) != (1 << SHIFT_BITS) && "please define SHIFT_BITS according to CPU bit accordingly");
    // ceiling to cpu bit width
    asked_size = ceiling_to_2_pow_n (asked_size, SHIFT_BITS);
#endif
    // &g_malloc_area = g_malloc_area

    // ************* editing find available area and return
    if (!g_root) {
        g_root = (allocated_memory_t *)g_malloc_area;
    }
    allocated_memory_t *iter = g_root;

    // g_malloc_area_end is included
    const uintptr_t malloc_area_end = (uintptr_t) (&g_malloc_area[MALLOC_SIZE]);
    assert (((uintptr_t) (&g_malloc_area[MALLOC_SIZE])) == (uintptr_t) g_malloc_area + sizeof (g_malloc_area));

    // g_malloc_area does not need to modify when using list

    if (asked_size >= sizeof (g_malloc_area)) {
        return (void *)0;
    }

    // if root is not the start of area, then calculate the size of first area
    if (iter != (allocated_memory_t *)g_malloc_area) {
        // i64_t available_size = (i64_t)(uintptr_t)iter - (uintptr_t)g_malloc_area
        //                        - sizeof (allocated_memory_t);
        // if ((i64_t)asked_size <= available_size) {

        // ugly code, to avoid minus and comparison between signed and unsigned int
        if (asked_size + (uintptr_t)g_malloc_area + sizeof (allocated_memory_t) <= (uintptr_t)iter) {
            allocated_memory_t *next = iter;
            iter = (allocated_memory_t *)g_malloc_area;
            iter->next_area = next;
            iter->size = asked_size;
            iter->self = (allocated_memory_t *) ((uintptr_t)iter + sizeof (allocated_memory_t));
            g_root = iter;
            return iter->self;
        }
    }

    while (1) {
        // if this is the last
        if (!iter->next_area) {
            // if this area is big enough
            // if this is root, size = all available area - sizeof(allocated_memory_t)
            // since it is to create a new element at the end

            // is it empty
            if (0 == iter->size) {
                // i64_t available_size = (i64_t) malloc_area_end - (u64_t)iter - 2 * sizeof (allocated_memory_t);
                // if ((i64_t)asked_size <= available_size) {
                allocated_memory_t *aligned_iter;
#ifdef MALLOC_ALIGN
                aligned_iter = (allocated_memory_t *)ceiling_to_2_pow_n ((uintptr_t)iter, SHIFT_BITS);
#else
                aligned_iter = iter;
#endif
                if (asked_size + (uintptr_t)aligned_iter + 2 * sizeof (allocated_memory_t) <= malloc_area_end) {
                    // next element is smaller than available since we already checked in available_size
                    // initialize next area
                    iter->next_area = (allocated_memory_t *) ((uintptr_t)aligned_iter + asked_size + sizeof (allocated_memory_t));
                    iter->size = asked_size; // set size to non-zero value
                    iter->self = (allocated_memory_t *) ((uintptr_t)aligned_iter + sizeof (allocated_memory_t));

                    iter->next_area->next_area = (void *)0;
                    iter->next_area->size = 0;
                    iter->next_area->self = (void *)0;

                    return iter->self;
                }
                // size not enough
                else {}
            }
            // not empty, last area should always be empty
            else {
                assert (0 && "area other than last should never be empty");
            }
        }
        else { // not the last area
            // area other than the last shoule never be empty
            // if this area is ont enough
            // i64_t available_size = (i64_t) (uintptr_t)iter->next_area - (u64_t)iter->self - iter->size
            //                        - sizeof (allocated_memory_t);
            // if ((i64_t)asked_size <= available_size) {

            allocated_memory_t *aligned_iter;
#ifdef MALLOC_ALIGN
            aligned_iter = (allocated_memory_t *)ceiling_to_2_pow_n ((uintptr_t)iter, SHIFT_BITS);
#else
            aligned_iter = iter;
#endif
            if (asked_size + (uintptr_t)aligned_iter + iter->size + 2 * sizeof (allocated_memory_t) <= (uintptr_t)iter->next_area) {
                allocated_memory_t *next = iter->next_area;
                iter->next_area = (allocated_memory_t *) ((uintptr_t) aligned_iter + sizeof (allocated_memory_t) + iter->size);

                iter->next_area->next_area = next;
                iter->next_area->size = asked_size;
                iter->next_area->self = (allocated_memory_t *) ((uintptr_t)iter->next_area + sizeof (allocated_memory_t));
                return iter->next_area->self;
            }
            // else // iterate to next and continue
        }

        // else
        if (iter->next_area) {
            iter = iter->next_area;
        }
        else {
            return (void *)0;
        }
    }
    return (void *)0; // return nullptr indicates error
}

// random
int g_initialized = 0;
u32_t shr3;

u32_t next() {
    if (!g_initialized) {
        shr3 = 0x1;
        g_initialized = 1;
    }

    shr3 ^= (shr3 << 13);
    shr3 ^= (shr3 >> 17);
    shr3 ^= (shr3 << 5);

    return shr3;
}


// *** for ARM ******************************************************************
void SystemInit (void) {
}

void WWDG_IRQHandler (void) {};
void UsageFault_Handler (void) {};

int main() {
#define ARRAY_LENGTH (10)
    unsigned char *a[ARRAY_LENGTH] = {0};
    int cnt = 0;
    while (1) {
        for (int i = 0; i < ARRAY_LENGTH; i++) {
            if (!a[i]) {
                u32_t malloc_size = 0xf0;
                a[i] = (unsigned char *)my_malloc (malloc_size);
                if (a[i]) {
                    unsigned char *p = a[i];
                    for (u32_t j = 0; j < malloc_size; j++) {
                        *p = 0x01;
                        p++;
                    }
                }
            }
        }

        for (int i = 0; i < ARRAY_LENGTH; i++) {
            if (next() < 0x80000000 && a[i]) {
                if (a[i]) {
                    my_free (a[i]);
                    a[i] = 0;
                }
            }
        }

        for (int i = 0; i < ARRAY_LENGTH; i++) {
            if (!a[i]) {
                u32_t malloc_size = 4;
                a[i] = (unsigned char *)my_malloc (malloc_size);
                if (a[i]) {
                    unsigned char *p = a[i];
                    for (u32_t j = 0; j < malloc_size; j++) {
                        *p = 0x01;
                        p++;
                    }
                }
            }
        }
        cnt ++;
    }
}
