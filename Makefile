PREFIX := /usr/bin/

CC := $(PREFIX)gcc
CXX := $(PREFIX)g++
LD := $(PREFIX)g++
AS := $(PREFIX)gcc -x assembler-with-cpp
CP := $(PREFIX)objcopy
AR := $(PREFIX)ar
SZ := $(PREFIX)size


# https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html

OVERALL_FLAGS := -fdata-sections -ffunction-sections

# C flags
# with all warnings, with extra warnings
CFLAGS := -Wall -Wextra -std=c99

# C preprocess flags
# CPPFLAGS := -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)"
CPPFLAGS :=

# Extra flags to give to the C++ compiler.
CXXFLAGS := -Wall -Wextra -std=c++14

# ******************************************************************************
TARGET := main
DEBUG := 1


TOP := $(shell pwd)/
SRC := .
INC := -I$(TOP) \
	-Iinclude
OBJ := obj

# libraries
LIB := -lc -lm

# ********** LDFLAGS **********
# link script
# LDSCRIPT = -T
LDSCRIPT :=

# -Wl,(XX) means send xx to linker
# -Wl,--gc-sections means send --gc-sections to linker
# -fdata-sections -ffunction-sections
# LDFLAGS := -Wl,--gc-sections -g -Wall --specs=nosys.specs # -specs=nano.specs
# --cref create reference table
LDFLAGS := $(MCU) $(LDSCRIPT) $(INC) $(LIB) $(STARTUP_DEFS) $(OVERALL_FLAGS) \
	-Wl,-Map=$(OBJ)/$(TARGET).map,--cref,--gc-sections

# -specs=nano.specs -fdata-sections -ffunction-sections
# compile gcc flags
ASFLAGS := $(ASFLAGS) $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) $(STARTUP_DEFS) $(OVERALL_FLAGS)

CFLAGS := $(CFLAGS) $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) $(STARTUP_DEFS) $(OVERALL_FLAGS)

CXXFLAGS := $(CXXFLAGS) $(MCU) $(CXX_DEFS) $(CXX_INCLUDES) $(OPT) $(STARTUP_DEFS) $(OVERALL_FLAGS)

ifeq ($(DEBUG), 1)
CFLAGS += -g -gdwarf-2
CXXFLAGS += -g -gdwarf-2
LDFLAGS += -g -gdwarf-2
endif

# the first target in makefile is the default target
# which means
# `make` is the same with `make all`

# target: dependencies
# the next line of target starts with tab

all_c_files := $(sort $(notdir $(shell find $(SRC) -name "*.c")))
all_o_files := $(addprefix $(OBJ)/,$(all_c_files:.c=.o))

all_cc_files := $(sort $(notdir $(shell find $(SRC) -name "*.cc")))
all_cc_o_files := $(addprefix $(OBJ)/,$(all_cc_files:.cc=.o))

all_asm_files := $(sort $(notdir $(shell find $(SRC) -name "*.s")))
all_asm_o_files := $(addprefix $(OBJ)/,$(all_asm_files:.s=.o))

# list of ASM program objects
all_o_files += $(all_asm_o_files) $(all_cc_o_files)

all_d_files := $(all_o_files:.o=.d)

# ******************************************************************************
.PHONY: clean all $(OBJ)

# default target: all
# no recipe for target all
all: $(OBJ)/$(TARGET).elf
.PHONY: all

# LD is not using implicit rule
$(OBJ)/%.elf: $(all_o_files) | $(OBJ)
	$(LD) $(all_o_files) $(LDFLAGS) -o $@

$(OBJ)/%.hex: $(OBJ)/%.elf
	$(HEX) $< $@

$(OBJ)/%.bin: $(OBJ)/%.elf
	$(BIN) $< $@


# $@ target
# $< first prerequisite
# $(@D) the directory part of $@
# $^ The names of all the prerequisites, with spaces between them
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
# https://www.gnu.org/software/make/manual/html_node/Automatic-Prerequisites.html
-include $(all_d_files)

# where to put OBJ is a question, sometimes compile will fail due to sequence error
$(OBJ):
	mkdir -p $(OBJ)

# TODO: fix asm .d generation
$(OBJ)/%.d: %.cc | $(OBJ)
	set -e; rm -f $@; \
	$(CXX) -MM $(CXXFLAGS) $(INC) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


$(OBJ)/%.d: %.c | $(OBJ)
	set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $(INC) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


$(OBJ)/%.d: %.s | $(OBJ)
	set -e; rm -f $@; \
	$(AS) -MM $(ASFLAGS) $(INC) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

# https://stackoverflow.com/questions/47447369/gnu-make-removing-intermediate-files
.PRECIOUS: $(OBJ)/%.o
$(OBJ)/%.o: %.cc | $(OBJ)
	$(CXX) $(CXXFLAGS) $(INC) -c $^ -o $@

$(OBJ)/%.o: %.c | $(OBJ)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@

$(OBJ)/%.o: %.s | $(OBJ)
	$(AS) -c $(ASLAGS) $< -o $@

%.o:
	echo "please use target obj/$@ instead"
	exit 1

%.d:
	echo "please use target obj/$@ instead"
	exit 1


# A phony target is one that is not really the name of a file
clean:
	rm -f $(TARGET)
	rm -rf $(OBJ)
	rm -f *.o
	rm -f *.d
	rm -f GTAGS GRTAGS GPATH

